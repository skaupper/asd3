cmake_minimum_required(VERSION 3.16)

project(ue01 CXX)

# Include all CMake scripts
include(cmake/StandardProjectSettings.cmake)
include(cmake/PreventInSourceBuilds.cmake)
include(cmake/StaticAnalyzers.cmake)
include(cmake/Sanitizers.cmake)
include(cmake/CompilerWarnings.cmake)


option(COMPILE_ORIGINAL_VERSION "" false)


# Place executables in a separate bin/ directory
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

# Symlink the compile_commands.json for VS Code
add_custom_target(link_compile_commands ALL
                  COMMAND ${CMAKE_COMMAND} -E create_symlink ${CMAKE_BINARY_DIR}/compile_commands.json ${CMAKE_SOURCE_DIR}/../build/compile_commands.json)


#
# Interface libraries
#
add_library(project_options INTERFACE)
add_library(project_warnings INTERFACE)

# Set options
target_compile_features(project_options INTERFACE cxx_std_17)
enable_sanitizers(project_options)

# Set relevant warnings
set_project_warnings(project_warnings)


#
# Compile targets
#
add_subdirectory(src)

if(${COMPILE_ORIGINAL_VERSION})
    add_subdirectory(CsvToJson)
endif()
