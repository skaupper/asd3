// Attention: This is the unmodified file from the task assignment!

#include "CsvTable.h"
#include <fstream>

using namespace std;

CsvTable::CsvTable(string const &fileName, char separator) : mSeparator(separator) {
    ifstream in(fileName);
    if (!in)
        throw runtime_error("Could not open input file.");
    string line;
    while (getline(in, line)) {
        if (!line.empty())
            parseLine(line);
    }
}

void CsvTable::parseLine(std::string const &line) {
    auto &row  = mRows.emplace_back();
    size_t pos = 0;
    size_t nextPos;
    do {
        nextPos = line.find(mSeparator, pos);
        row.emplace_back(line, pos, nextPos - pos);
        pos = nextPos + 1;
    } while (nextPos < line.size());
}
