// Attention: This is the unmodified file from the task assignment!

#ifndef CSVTABLE_H
#define CSVTABLE_H

#include <string>
#include <vector>

class CsvTable {
public:
    using Element = std::string;
    using Row     = std::vector<Element>;

    explicit CsvTable(std::string const &fileName, char separator = ';');

    auto &getRows() const { return mRows; }
    size_t getNrRows() const { return mRows.size(); }

    auto begin() const { return mRows.begin(); }
    auto end() const { return mRows.end(); }

private:
    std::vector<Row> mRows;
    char mSeparator;

    void parseLine(std::string const &line);
};

#endif
