// Attention: This is the unmodified file from the task assignment!

#include "CsvTable.h"
#include <chrono>
#include <fstream>
#include <iostream>

using namespace std;
using namespace std::chrono;

void writeJson(std::string const &fileName, CsvTable const &table) {
    (void)table;
    (void)fileName;
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        cerr << "Missing input file." << endl;
        return 1;
    }
    try {
        auto start     = high_resolution_clock::now();
        string csvName = argv[1];
        CsvTable table(csvName);
        double duration = chrono::duration<double>(high_resolution_clock::now() - start).count();
        cout << "Read " << table.getNrRows() << " rows in " << duration << " seconds." << endl;
        string jsonName = argc < 3 ? csvName + ".json" : argv[2];
        writeJson(jsonName, table);
    } catch (exception const &e) {
        cerr << "Error: " << e.what() << endl;
        return 1;
    }
    return 0;
}
