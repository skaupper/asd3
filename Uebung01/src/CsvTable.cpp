#include "CsvTable.h"
#include <charconv>
#include <fstream>


CsvTable::CsvTable(const std::string &fileName, const char separator) : mSeparator(separator) {
    readFileContent(fileName);
    splitFileContent();
}

void CsvTable::readFileContent(const std::string &fileName) {
    std::ifstream in(fileName);
    if (!in) {
        throw std::runtime_error("Could not open input file.");
    }

    in.seekg(0, std::ios::end);
    mFileContent.resize(static_cast<size_t>(in.tellg()));
    in.seekg(0, std::ios::beg);
    in.read(std::data(mFileContent), static_cast<std::streamsize>(std::size(mFileContent)));
}


static std::string_view getNextLine(std::string_view &content) {
    const size_t crlfIndex {content.find_first_of("\r\n")};  // 23.38%
    if (crlfIndex == std::string_view::npos) {
        std::string_view line;
        line.swap(content);
        return line;
    }

    const std::string_view line {std::data(content), crlfIndex};
    content.remove_prefix(crlfIndex);

    // That might stretch the requirements a bit, since multiple line breaks
    // (as well as odd combinations of \r and \n) are removed as well.
    const auto startOfNextLine {content.find_first_not_of("\r\n")};
    if (startOfNextLine != std::string_view::npos) {
        content.remove_prefix(startOfNextLine);
    } else {
        content.remove_prefix(std::size(content));
    }
    return line;
}

static std::string_view getNextColumn(std::string_view &line, bool &columnsRemaining, const char separator) {
    const size_t sepIndex {line.find_first_of(separator)};
    if (sepIndex == std::string_view::npos) {
        std::string_view col;
        col.swap(line);
        columnsRemaining = false;
        return col;
    }

    const std::string_view col {std::data(line), sepIndex};
    line.remove_prefix(sepIndex + 1);
    columnsRemaining = true;
    return col;
}

static CsvTable::Element parseElement(const std::string_view column) {

    if (std::size(column) >= 2 && (column.front() == '"' && column.back() == '"')) {
        // The string is enclosed with double quotes
        return {std::string_view {std::data(column) + 1, std::size(column) - 2}};
    }

    CsvTable::Number number;
    const auto last {std::data(column) + std::size(column)};
    const auto [ptr, ec] = std::from_chars(std::data(column), last, number);  // 18.11%

    if (ptr == last && ec == std::errc {}) {
        // The whole column has been matched as a number
        return number;
    }

    return column;
}


void CsvTable::splitFileContent() {
    constexpr size_t INITIAL_ROW_CAPACITY {5};


    std::string_view remainingContent {mFileContent};

    while (!remainingContent.empty()) {
        mRows.emplace_back();  // 9.94%

        Row &currentRow {mRows.back()};
        currentRow.reserve(INITIAL_ROW_CAPACITY);

        std::string_view line {getNextLine(remainingContent)};

        bool columnsRemaining {true};
        while (columnsRemaining) {
            std::string_view column {getNextColumn(line, columnsRemaining, mSeparator)};
            currentRow.emplace_back(parseElement(column));  // 9.04%
        }
    }
}
