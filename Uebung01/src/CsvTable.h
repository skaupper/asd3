#ifndef CSVTABLE_H
#define CSVTABLE_H

#include <string>
#include <string_view>
#include <variant>
#include <vector>


class CsvTable {
public:
    using Number  = long long;
    using Element = std::variant<std::string_view, Number>;
    using Row     = std::vector<Element>;

    explicit CsvTable(const std::string &fileName, const char separator = ';');

    auto &getRows() const { return mRows; }
    size_t getNrRows() const { return mRows.size(); }

    auto begin() { return mRows.begin(); }
    auto end() { return mRows.end(); }
    auto begin() const { return mRows.cbegin(); }
    auto end() const { return mRows.cend(); }

private:
    std::string mFileContent;
    std::vector<Row> mRows;
    char mSeparator;

    void readFileContent(const std::string &fileName);
    void splitFileContent();
};

#endif
