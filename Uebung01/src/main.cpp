#include "CsvTable.h"

#include <chrono>
#include <fstream>
#include <iostream>
#include <iterator>
#include <optional>


//
// Argument section
//

struct Arguments {
    std::string inputFilename;
    std::string outputFilename;
};

static std::optional<Arguments> parseArgs(int argc, char *argv[]) {
    if (argc < 2 || argc > 3) {
        std::cerr << "Usage: " << argv[0] << " <inputFile> [<outputFile>]" << std::endl;
        return std::nullopt;
    }

    Arguments args;
    args.inputFilename = argv[1];
    if (argc == 3) {
        args.outputFilename = argv[2];
    } else {
        args.outputFilename = args.inputFilename + ".json";
    }
    return args;
}


//
// JSON section
//

// This template struct + corresponding deduction guide allow a set of lambdas to overload each other.
// This way multiple lambdas with different parameter types can be passed to std::visit.
template<class... Ts>
struct overload : Ts... {
    using Ts::operator()...;
};

template<class... Ts>
overload(Ts...) -> overload<Ts...>;


static void writeJsonFile(const std::string &fileName, const CsvTable &table) {
    std::ofstream out {fileName};
    if (!out) {
        throw std::runtime_error("Failed to open output file!");
    }

    out << "[";


    bool firstRow {true};
    for (const auto &row : table) {
        if (!firstRow) {
            out << ",";
        }
        firstRow = false;


        out << std::endl << "  [";


        bool firstCol {true};
        for (const auto &ele : row) {
            if (!firstCol) {
                out << ", ";
            }
            firstCol = false;

            std::visit(overload {
                           [&out](const std::string_view &e) { out << "\"" << e << "\""; },
                           [&out](const CsvTable::Number e) {
                               out << e;
                           }  //
                       },
                       ele);
        }

        out << "]";
    }

    out << std::endl;
    out << "]" << std::endl;
}


//
// MAIN
//

int main(int argc, char *argv[]) {

    const auto optArgs {parseArgs(argc, argv)};
    if (!optArgs) {
        return 1;
    }
    const auto args {*optArgs};


    try {
        const auto start = std::chrono::high_resolution_clock::now();
        CsvTable table(args.inputFilename);
        const double duration =
            std::chrono::duration<double>(std::chrono::high_resolution_clock::now() - start).count();

        std::cout << "Read " << table.getNrRows() << " rows in " << duration << " seconds." << std::endl;

        writeJsonFile(args.outputFilename, table);
    } catch (const std::exception &e) {
        std::cerr << "Error: " << e.what() << std::endl;
        return 1;
    }
    return 0;
}
