#ifndef JSON_WRITER_H
#define JSON_WRITER_H

#include <ostream>
#include <type_traits>
#include <string_view>

namespace JSON {

  class Object {
  public:
    virtual ~Object() = default;

    virtual void writeJson(std::ostream &out) const = 0;
  };

  template <typename T>
  constexpr bool is_char_v = std::is_same_v<T, char> || std::is_same_v<T, signed char> || std::is_same_v<T, unsigned char>;

  template <typename T>
  void writeValue(std::ostream &out, T const &val) {
    using type = typename std::remove_reference<T>::type;
    // TODO
  }

  template <typename... TArgs>
  void writeArray(std::ostream &out, TArgs &&...elems) {
    // TODO
  }

  template <typename T>
  void writeObjectElement(std::ostream &out, std::string_view name, T &&value) {
    out << '\"' << name << "\": ";
    writeValue(out, std::forward<T>(value));
  }

  template <typename... TArgs>
  void writeObject(std::ostream &out, TArgs &&...elems) {
    // TODO
  }
  
}

#endif

