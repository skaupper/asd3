#ifndef SET_H
#define SET_H

#include <bitset>
#include <iostream>
#include <limits>
#include <set>


// ------------------------------------------------------------------------------------------------
// GenericSet
// ------------------------------------------------------------------------------------------------


template<typename T, typename TPred = std::less<T>>
class GenericSet {
public:
    bool Add(const T &x) { return mSet.insert(x).second; }
    bool Remove(const T &x) { return mSet.erase(x) > 0; }
    bool Contains(const T &x) const { return mSet.find(x) != mSet.cend(); }
    size_t Count() const { return mSet.size(); }

    void Print(std::ostream &out) const;

    friend std::ostream &operator<<(std::ostream &out, const GenericSet<T, TPred> &x) {
        x.Print(out);
        return out;
    }

private:
    std::set<T, TPred> mSet;
};


template<typename T, typename TPred>
void GenericSet<T, TPred>::Print(std::ostream &out) const {
    out << "{";
    bool first {true};
    for (const auto &e : mSet) {
        if (!first) {
            out << ", ";
        }
        first = false;

        if constexpr (std::is_pointer_v<T> && std::is_arithmetic_v<std::remove_pointer_t<T>>) {
            out << +*e;
        } else if constexpr (std::is_pointer_v<T>) {
            out << *e;
        } else if constexpr (std::is_arithmetic_v<T>) {
            out << +e;
        } else {
            out << e;
        }
    }
    out << "}";
}


// ------------------------------------------------------------------------------------------------


// ------------------------------------------------------------------------------------------------
// GenericCharSet
// ------------------------------------------------------------------------------------------------


template<typename T>
class GenericCharSet {
public:
    using ElementType = std::remove_pointer_t<T>;

    bool Add(const T x);
    bool Remove(const T x);
    bool Contains(const T x) const { return mSet.test(toUnsigned(x)); }
    size_t Count() const { return mSet.count(); }

    void Print(std::ostream &out) const;

    friend std::ostream &operator<<(std::ostream &out, const GenericCharSet<T> &x) {
        x.Print(out);
        return out;
    }

private:
    template<typename U>
    static auto toUnsigned(const U x) {
        if constexpr (std::is_pointer_v<U>) {
            return static_cast<std::make_unsigned_t<std::remove_pointer_t<U>>>(*x);
        } else {
            return static_cast<std::make_unsigned_t<U>>(x);
        }
    }
    static constexpr auto MIN_VALUE {std::numeric_limits<ElementType>::min()};
    static constexpr auto MAX_VALUE {std::numeric_limits<ElementType>::max()};
    static constexpr auto WIDTH {MAX_VALUE - MIN_VALUE + 1};
    std::bitset<WIDTH> mSet;
};


template<typename T>
bool GenericCharSet<T>::Add(const T x) {
    if (mSet.test(toUnsigned(x))) {
        return false;
    }
    mSet.set(toUnsigned(x));
    return true;
}

template<typename T>
bool GenericCharSet<T>::Remove(const T x) {
    if (!mSet.test(toUnsigned(x))) {
        return false;
    }
    mSet.reset(toUnsigned(x));
    return true;
}

template<typename T>
void GenericCharSet<T>::Print(std::ostream &out) const {
    out << "{";

    bool first {true};
    ElementType nextValue {MIN_VALUE};
    do {
        const auto idx {toUnsigned(nextValue)};
        if (!mSet.test(idx)) {
            continue;
        }

        if (!first) {
            out << ", ";
        }
        first = false;
        out << +nextValue;
    } while (nextValue++ != MAX_VALUE);

    out << "}";
}


// ------------------------------------------------------------------------------------------------


// ------------------------------------------------------------------------------------------------
// Set
// ------------------------------------------------------------------------------------------------


template<typename T>
struct ComparePtr {
    constexpr bool operator()(const T *e1, const T *e2) const { return *e1 < *e2; }
};


template<typename T>
class Set : public GenericSet<T> {};

template<typename T>
class Set<T *> : public GenericSet<T *, ComparePtr<T>> {};

// C++20:
// class Set<T *> : public GenericSet<T *, decltype([](const T *e1, const T *e2){
//     return *e1 < *e2;})> {};


template<>
class Set<signed char> : public GenericCharSet<signed char> {};
template<>
class Set<signed char *> : public GenericCharSet<signed char *> {};

template<>
class Set<unsigned char> : public GenericCharSet<unsigned char> {};
template<>
class Set<unsigned char *> : public GenericCharSet<unsigned char *> {};


#endif
