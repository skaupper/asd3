#include "Set.h"

#include <cassert>
#include <sstream>


void testSignedCharSet() {
    std::cout << "Start signed char test..." << std::endl;


    Set<signed char> set;

    std::stringstream ss;
    ss << set;
    std::cout << ss.str() << std::endl;
    assert(ss.str() == "{}");


    set.Add(0);
    set.Add(127);
    set.Add(-128);


    assert(set.Contains(0) && set.Contains(127) && set.Contains(-128));
    assert(set.Count() == 3);
    assert(!set.Add(127));
    assert(set.Count() == 3);

    ss.str("");
    ss << set;
    std::cout << ss.str() << std::endl;
    assert(ss.str() == "{-128, 0, 127}");


    assert(set.Remove(0));
    assert(set.Remove(-128));
    assert(set.Count() == 1);

    ss.str("");
    ss << set;
    std::cout << ss.str() << std::endl;
    assert(ss.str() == "{127}");


    // Quick overflow test
    assert(!set.Contains(-1));
    set.Add(static_cast<signed char>(255));
    assert(set.Contains(-1));
    set.Remove(static_cast<signed char>(255));
    assert(!set.Contains(-1));

    std::cout << "Test successful!" << std::endl;
    std::cout << std::endl;
}

void testSignedCharPtrSet() {
    std::cout << "Start signed char pointer test..." << std::endl;


    Set<signed char *> set;

    std::stringstream ss;
    ss << set;
    std::cout << ss.str() << std::endl;
    assert(ss.str() == "{}");

    signed char values[] {0, 127, -128};
    signed char values2[] {0, 127, -128};


    set.Add(&values[0]);
    set.Add(&values[1]);
    set.Add(&values[2]);


    assert(set.Contains(&values2[0]) && set.Contains(&values2[1]) && set.Contains(&values2[2]));
    assert(set.Count() == 3);
    assert(!set.Add(&values2[1]));
    assert(set.Count() == 3);

    ss.str("");
    ss << set;
    std::cout << ss.str() << std::endl;
    assert(ss.str() == "{-128, 0, 127}");


    assert(set.Remove(&values2[0]));
    assert(set.Remove(&values[2]));
    assert(set.Count() == 1);

    ss.str("");
    ss << set;
    std::cout << ss.str() << std::endl;
    assert(ss.str() == "{127}");


    std::cout << "Test successful!" << std::endl;
    std::cout << std::endl;
}

void testUnsignedCharSet() {
    std::cout << "Start unsigned char test..." << std::endl;


    Set<unsigned char> set;

    std::stringstream ss;
    ss << set;
    std::cout << ss.str() << std::endl;
    assert(ss.str() == "{}");


    set.Add(0);
    set.Add(127);
    set.Add(255);

    assert(set.Contains(0) && set.Contains(127) && set.Contains(255));
    assert(set.Count() == 3);
    assert(!set.Add(127));
    assert(set.Count() == 3);

    ss.str("");
    ss << set;
    std::cout << ss.str() << std::endl;
    assert(ss.str() == "{0, 127, 255}");


    assert(set.Remove(0));
    assert(set.Remove(255));
    assert(set.Count() == 1);

    ss.str("");
    ss << set;
    std::cout << ss.str() << std::endl;
    assert(ss.str() == "{127}");


    // Quick overflow test
    assert(!set.Contains(254));
    set.Add(static_cast<unsigned char>(-2));
    assert(set.Contains(254));
    set.Remove(static_cast<unsigned char>(-2));
    assert(!set.Contains(254));


    std::cout << "Test successful!" << std::endl;
    std::cout << std::endl;
}


void testIntSet() {
    std::cout << "Start int test..." << std::endl;


    Set<int> set;

    std::stringstream ss;
    ss << set;
    std::cout << ss.str() << std::endl;
    assert(ss.str() == "{}");


    set.Add(-123456);
    set.Add(127);
    set.Add(+54645978);

    assert(set.Contains(-123456) && set.Contains(127) && set.Contains(54645978));
    assert(set.Count() == 3);
    assert(!set.Add(127));
    assert(set.Count() == 3);

    ss.str("");
    ss << set;
    std::cout << ss.str() << std::endl;
    assert(ss.str() == "{-123456, 127, 54645978}");


    assert(set.Remove(-123456));
    assert(set.Remove(54645978));
    assert(set.Count() == 1);

    ss.str("");
    ss << set;
    std::cout << ss.str() << std::endl;
    assert(ss.str() == "{127}");


    std::cout << "Test successful!" << std::endl;
    std::cout << std::endl;
}


void testIntPtrSet() {
    std::cout << "Start int pointer test..." << std::endl;


    Set<const int *> set;
    const int values[] {-123, 0, 4711};
    const int values2[] {-123, 0, 4711};

    std::stringstream ss;
    ss << set;
    std::cout << ss.str() << std::endl;
    assert(ss.str() == "{}");


    set.Add(&values[0]);
    set.Add(&values[1]);
    set.Add(&values[2]);

    assert(set.Contains(&values2[0]) && set.Contains(&values2[1]) && set.Contains(&values2[2]));
    assert(set.Count() == 3);
    assert(!set.Add(&values2[1]));
    assert(set.Count() == 3);

    ss.str("");
    ss << set;
    std::cout << ss.str() << std::endl;
    assert(ss.str() == "{-123, 0, 4711}");


    assert(set.Remove(&values2[0]));
    assert(set.Remove(&values2[2]));
    assert(set.Count() == 1);

    ss.str("");
    ss << set;
    std::cout << ss.str() << std::endl;
    assert(ss.str() == "{0}");


    std::cout << "Test successful!" << std::endl;
    std::cout << std::endl;
}


int main() {
    testSignedCharSet();
    testSignedCharPtrSet();
    testUnsignedCharSet();
    testIntSet();
    testIntPtrSet();

    return 0;
}
