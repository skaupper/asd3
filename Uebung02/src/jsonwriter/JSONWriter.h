#ifndef JSON_WRITER_H
#define JSON_WRITER_H

#include <ostream>
#include <string_view>
#include <type_traits>


namespace JSON {

    class Object {
    public:
        virtual ~Object() = default;

        virtual void writeJson(std::ostream &out) const = 0;
    };

    template<typename T>
    constexpr bool is_char_v =
        std::is_same_v<T, char> || std::is_same_v<T, signed char> || std::is_same_v<T, unsigned char>;

    template<typename T>
    constexpr bool is_bool_v = std::is_same_v<T, bool>;

    template<typename T>
    void writeValue(std::ostream &out, const T &val) {
        using type = typename std::remove_reference<T>::type;
        if constexpr (std::is_arithmetic_v<type> && !is_char_v<type> && !is_bool_v<type>) {
            out << val;
        } else if constexpr (is_bool_v<type>) {
            out << (val ? "true" : "false");
        } else if constexpr (std::is_pointer_v<type>) {
            if (val) {
                writeValue(out, *val);
            } else {
                out << "null";
            }
        } else if constexpr (std::is_null_pointer_v<type>) {
            out << "null";
        } else if constexpr (std::is_base_of_v<Object, type>) {
            val.writeJson(out);
        } else {
            out << '"' << val << '"';
        }
    }

    template<typename T, typename... TArgs>
    void writeArrayElement(std::ostream &out, T &&ele, TArgs &&... elems) {
        writeValue(out, std::forward<T>(ele));
        if constexpr (sizeof...(elems) > 0) {
            out << ", ";
            writeArrayElement(out, std::forward<TArgs>(elems)...);
        }
    }

    template<typename... TArgs>
    void writeArray(std::ostream &out, TArgs &&... elems) {
        out << "[";
        if constexpr (sizeof...(elems) > 0) {
            writeArrayElement(out, std::forward<TArgs>(elems)...);
        }
        out << "]";
    }

    template<typename T>
    void writeObjectElement(std::ostream &out, std::string_view name, T &&value) {
        out << '\"' << name << "\": ";
        writeValue(out, std::forward<T>(value));
    }

    template<typename T, typename... TArgs>
    void writeObjectElement(std::ostream &out, std::string_view name, T &&value, TArgs &&... elems) {
        writeObjectElement(out, name, std::forward<T>(value));
        out << ", ";
        writeObjectElement(out, std::forward<TArgs>(elems)...);
    }

    template<typename... TArgs>
    void writeObject(std::ostream &out, TArgs &&... elems) {
        out << "{";
        if constexpr (sizeof...(elems) > 0) {
            writeObjectElement(out, std::forward<TArgs>(elems)...);
        }
        out << "}";
    }

}  // namespace JSON

#endif
