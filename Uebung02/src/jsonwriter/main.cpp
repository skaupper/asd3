#include "JSONWriter.h"
#include <iostream>
#include <string>

using namespace JSON;


class Person : public Object {
public:
    Person(std::string name, int age) : mName(std::move(name)), mAge(age) {}

    void writeJson(std::ostream &out) const override { writeObject(out, "name", mName, "age", mAge); }

private:
    const std::string mName;
    const int mAge;
};


template<typename... TArgs>
void writeArrayLn(TArgs &&... args) {
    writeArray(std::cout, std::forward<TArgs>(args)...);
    std::cout << std::endl;
}

template<typename... TArgs>
void writeObjectLn(TArgs &&... args) {
    writeObject(std::cout, std::forward<TArgs>(args)...);
    std::cout << std::endl;
}

int main() {
    writeArrayLn();                                                             // []
    writeArrayLn(1);                                                            // [1]
    writeArrayLn(1, 1.5, 2, 3, 4.4f);                                           // [1, 1.5, 2, 3, 4.4]
    writeArrayLn(1, false, true, "abc", 'a', static_cast<unsigned char>('b'));  // [1, false, true, "abc", "a", "b"]

    int x = 0;
    writeArrayLn(&x, nullptr);      // [0, null]
    writeObjectLn("x", 1, "y", 2);  // {"x": 1, "y": 2 }

    Person joe("Joe", 40);
    joe.writeJson(std::cout);  // {"name": "Joe", "age": 40 }
    std::cout << std::endl;

    Person jack("Jack", 30);
    writeArrayLn(joe, jack);  // [{"name": "Joe", "age": 40}, {"name": "Jack", "age": 30}]

    writeObjectLn();          // {}
    writeObjectLn("1", "2");  // {"1": "2"}

    Person *nobody = nullptr;
    writeArrayLn(&joe, &jack, nobody);  // [{"name": "Joe", "age": 40}, {"name": "Jack", "age": 30}, null]
    std::cout << std::endl;
    return 0;
}
