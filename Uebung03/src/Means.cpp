#include "Means.h"
#include <algorithm>
#include <cassert>
#include <cmath>
#include <functional>
#include <future>
#include <thread>


bool ComputeMeans(const Numbers &numbers, Means &averages) {
    if (numbers.empty()) {
        return false;
    }

    double sum           = 0;
    double logSum        = 0;
    double reciprocalSum = 0;
    double quadSum       = 0;
    for (const auto nr : numbers) {
        sum += nr;
        logSum += log(nr);
        reciprocalSum += 1.0 / nr;
        quadSum += nr * nr;
    }

    const double size       = static_cast<double>(numbers.size());
    averages.arithmeticMean = sum / size;
    averages.geometricMean  = exp(logSum / size);
    averages.harmonicMean   = size / reciprocalSum;
    averages.quadraticMean  = sqrt(quadSum / size);
    return true;
}


bool ParallelComputeMeans(const Numbers &numbers, Means &averages, int nrThreads) {
    if (numbers.empty()) {
        return false;
    }

    using Sums = Means;

    const size_t actNrOfThreads {nrThreads == 0 ? std::thread::hardware_concurrency() : static_cast<size_t>(nrThreads)};
    const size_t size {std::size(numbers)};


    constexpr auto computePartialResult = [](const auto first, const auto last) {
        Sums sums {};
        for (auto it {first}; it != last; ++it) {
            sums.arithmeticMean += *it;
            sums.geometricMean += log(*it);
            sums.harmonicMean += 1.0 / *it;
            sums.quadraticMean += *it * *it;
        }
        return sums;
    };


    // Calculate partial results asynchronously
    std::vector<std::future<Sums>> results;

    auto currIt {std::begin(numbers)};
    for (size_t tid {0}; tid < actNrOfThreads - 1; ++tid) {
        const size_t loadPerThread {size / actNrOfThreads + static_cast<size_t>(tid >= size % actNrOfThreads)};
        auto lastIt {currIt};
        std::advance(lastIt, loadPerThread);

        results.emplace_back(std::async(std::launch::async, computePartialResult, currIt, lastIt));

        currIt = lastIt;
    }

    // Combine partial results
    Sums sums {computePartialResult(currIt, std::end(numbers))};
    for (auto &f : results) {
        const auto partResult {f.get()};
        sums.arithmeticMean += partResult.arithmeticMean;
        sums.geometricMean += partResult.geometricMean;
        sums.harmonicMean += partResult.harmonicMean;
        sums.quadraticMean += partResult.quadraticMean;
    }

    averages.arithmeticMean = sums.arithmeticMean / static_cast<double>(size);
    averages.geometricMean  = exp(sums.geometricMean / static_cast<double>(size));
    averages.harmonicMean   = static_cast<double>(size) / sums.harmonicMean;
    averages.quadraticMean  = sqrt(sums.quadraticMean / static_cast<double>(size));


    return true;
}
