#ifndef AVERAGE_H
#define AVERAGE_H

#include <vector>


using Numbers = std::vector<int>;

struct Means {
    double arithmeticMean;
    double geometricMean;
    double harmonicMean;
    double quadraticMean;
};

bool ComputeMeans(const Numbers &numbers, Means &averages);

bool ParallelComputeMeans(const Numbers &numbers, Means &averages, int nrThreads = 0);


#endif
