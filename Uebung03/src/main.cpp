#include <algorithm>
#include <chrono>
#include <ctime>
#include <functional>
#include <iostream>
#include <iterator>
#include <random>

#include "Means.h"


constexpr size_t smallN = 20;
constexpr size_t largeN = 100000000;


static void InitRandom(Numbers &vec, const size_t nr, const int max) {
    const auto rgen = std::bind(std::uniform_int_distribution<int>(1, max + 1),
                          std::default_random_engine(static_cast<unsigned int>(time(0))));
    vec.reserve(nr);
    std::generate_n(back_inserter(vec), nr, rgen);
}

static void PrintNumbers(const Numbers &numbers) {
    std::cout << "Numbers: ";
    std::copy(numbers.begin(), numbers.end(), std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;
}

static void PrintMeans(const Means &avg) {
    std::cout << "Arithmetic mean: " << avg.arithmeticMean << std::endl
              << "Geometric mean: " << avg.geometricMean << std::endl
              << "Harmonic mean: " << avg.harmonicMean << std::endl
              << "Quadratic mean: " << avg.quadraticMean << std::endl;
}

static void TestFunctional() {
    Numbers numbers;
    InitRandom(numbers, smallN, smallN);
    Means avg = {};
    ComputeMeans(numbers, avg);
    PrintNumbers(numbers);
    std::cout << "\nComputeMeans:\n";
    PrintMeans(avg);
    std::cout << "\nParallelComputeMeans:\n";
    Means pavg = {};
    ParallelComputeMeans(numbers, pavg);
    PrintMeans(pavg);
}


#ifdef NDEBUG
static void TestRuntime() {
    Numbers numbers;
    InitRandom(numbers, largeN, largeN);
    std::cout << "\nRuntime:\n";
    Means avg {};
    auto startTime = std::chrono::high_resolution_clock::now();
    ComputeMeans(numbers, avg);
    auto endTime = std::chrono::high_resolution_clock::now();
    double time1 = std::chrono::duration<double>(endTime - startTime).count();
    std::cout << "ComputeMeans: " << time1 << std::endl;
    Means pavg = {};
    startTime  = std::chrono::high_resolution_clock::now();
    ParallelComputeMeans(numbers, pavg);
    endTime      = std::chrono::high_resolution_clock::now();
    double time2 = std::chrono::duration<double>(endTime - startTime).count();
    std::cout << "ParallelComputeMeans: " << time2 << " (Speedup: " << time1 / time2 << ")\n";
}
#endif


int main() {
    TestFunctional();
#ifdef NDEBUG
    TestRuntime();
#endif
    return 0;
}
