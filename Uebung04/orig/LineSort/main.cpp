#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

bool ExtractNumbers(std::string const &str, std::vector<int> &numbers) {
  long long number = 0;
  bool isNumber = false;
  for (size_t i = 0; i < str.size(); i++) {
    char ch = str[i];
    if (ch == ' ' || ch == '\t' || ch == '\r' || ch == '\n') {
      if (isNumber) {
        numbers.push_back(int(number));
        isNumber = false;
        number = 0;
      }
    }
    else if (ch >= '0' && ch <= '9') {
      isNumber = true;
      number = number * 10 + ch - '0';
      if (number > numeric_limits<int>::max()) {
        return false;
      }
    }
    else {
      return false;
    }
  }
  if (isNumber) {
    numbers.push_back((int)number);
  }
  return true;
}

std::string ConvertToString(std::vector<int> const &numbers) {
  std::string str;
  str.reserve(numbers.size() * 11);
  for (int number : numbers) {
    int d = 1;
    while (d <= number / 10) {
      d *= 10;
    }
    while (d > 0) {
      str += char(number / d + '0');
      number %= d;
      d /= 10;
    }
    str += ' ';
  }
  return str;
}

void Run(string const &inputName, string const &outputName) {
  ifstream in(inputName);
  ofstream out(outputName);
  string line;
  while (getline(in, line)) {
    vector<int> numbers;
    if (ExtractNumbers(line, numbers)) {
      sort(numbers.begin(), numbers.end());
      out << ConvertToString(numbers) << endl;
    }
    else {
      out << "*** error ***" << endl;
    }
  }
}

int main(int argc, char *argv[]) {
  if (argc < 3) {
    cout << "Not enough arguments.\n";
    return 1;
  }
  Run(argv[1], argv[2]);
  return 0;
}
