#ifndef RADIXSORT_H
#define RADIXSORT_H

#include <vector>
#include <iterator>
#include <algorithm>
#include <functional>
#include <memory>
#include <climits>
#include <cassert>
#include <tbb/task_group.h>
#include <tbb/parallel_for.h>
#include <tbb/parallel_reduce.h>

namespace sorting {

  namespace internal {

    size_t const radixBits = 8;
    size_t const radix = 1 << radixBits;

    template <typename TIter, typename TIter2>
    void RadixSort(TIter begin, TIter end, TIter2 temp, size_t digit) {
      using TValue = typename std::iterator_traits<TIter>::value_type;
      size_t size = end - begin;
      if (size > 1) {
        size_t count[radix] = {};
        for (TIter iter = begin; iter != end; ++iter) {  // count occurrences
          assert(*iter >= 0); // this implementation only works for positive numbers
          count[(*iter >> digit) & (radix - 1)]++;
        }
        size_t pos[radix];  // positions of buckets in temporary array
        pos[0] = 0;
        for (size_t i = 0; i < radix - 1; i++) {  // compute starting positions of buckets
          pos[i + 1] = pos[i] + count[i];
        }
        for (TIter iter = begin; iter != end; iter++) { // put numbers into buckets
          TValue value = *iter;
          size_t index = (value >> digit) & (radix - 1);
          *(temp + pos[index]) = value;
          pos[index]++;
        }
        std::copy(temp, temp + size, begin);  // copy contents of buckets back to container
        if (digit > 0) {
          size_t nextDigit = digit - radixBits;
          TIter iter = begin;
          TIter2 iterTemp = temp;
          for (size_t i = 0; i < radix; i++) {  // recursively sort subranges
            size_t nr = count[i];
            RadixSort(iter, iter + nr, iterTemp, nextDigit);
            iter += nr;
            iterTemp += nr;
          }
        }
      }
    }

  } // namespace internal

  template <typename TIter>
  void RadixSort(TIter begin, TIter end) {
    using TValue = typename std::iterator_traits<TIter>::value_type;
    if (begin != end) {
      size_t size = end - begin;
      if (size > 0) {
        std::unique_ptr<TValue[]> temp(new TValue[size]);
        internal::RadixSort(begin, end, temp.get(), sizeof(TValue) * CHAR_BIT - internal::radixBits);
      }
    }
  }

  namespace internal {

    template <typename TIter, typename TIter2>
    void ParallelRadixSort(TIter begin, TIter end, TIter2 temp, size_t digit) {
      size_t const grainSize = 10000;
      if (end - begin < grainSize) {
        RadixSort(begin, end, temp, digit);
      }
      else {
        // TODO
      }
    }

  } // namespace internal

  template <typename TIter>
  void ParallelRadixSort(TIter begin, TIter end) {
    using TValue = typename std::iterator_traits<TIter>::value_type;
    if (begin != end) {
      size_t size = end - begin;
      if (size > 0) {
        std::unique_ptr<TValue[]> temp(new TValue[size]);
        internal::ParallelRadixSort(begin, end, temp.get(), sizeof(TValue) * CHAR_BIT - internal::radixBits);
      }
    }
  }

} // namespace sorting

#endif
