#ifndef MEANS_H
#define MEANS_H

#include <vector>

using Numbers = std::vector<int>;

struct Means {
  int mode;
  double median;
  double arithmeticMean;
  double geometricMean;
  double harmonicMean;
  double quadraticMean;
};

bool ComputeMeans(Numbers const &numbers, Means &averages);

bool ParallelComputeMeans(Numbers const &numbers, Means &averages);

#endif
