#include <iostream>
#include <algorithm>
#include <iterator>
#include <functional>
#include <ctime>
#include <random>
#include <chrono>
#include "Means.h"

using namespace std;
using namespace std::chrono;

size_t const smallN = 20;
size_t const largeN = 100000000;

void InitRandom(Numbers &vec, size_t const nr, int max) {
  auto rgen = std::bind(uniform_int_distribution<int>(1, max + 1), default_random_engine((unsigned int)time(0)));
  vec.reserve(nr);
  generate_n(back_inserter(vec), nr, rgen);
}

void PrintNumbers(Numbers const &numbers) {
  cout << "Numbers: ";
  copy(numbers.begin(), numbers.end(), ostream_iterator<int>(cout, " "));
  cout << endl;
}

void PrintMeans(Means const &means) {
  cout << "Arithmetic mean: " << means.arithmeticMean << endl
    << "Geometric mean: " << means.geometricMean << endl
    << "Harmonic mean: " << means.harmonicMean << endl
    << "Quadratic mean: " << means.quadraticMean << endl;
}

void TestFunctional() {
  Numbers numbers;
  InitRandom(numbers, smallN, smallN);
  Means means = {};
  ComputeMeans(numbers, means);
  PrintNumbers(numbers);
  cout << "\nComputeMeans:\n";
  PrintMeans(means);
  cout << "\nParallelComputeMeans:\n";
  Means pmeans = {};
  ParallelComputeMeans(numbers, pmeans);
  PrintMeans(pmeans);
}

void TestRuntime() {
  Numbers numbers;
  InitRandom(numbers, largeN, largeN);
  cout << "\nRuntime:\n";
  Means means {};
  auto startTime = high_resolution_clock::now();
  ComputeMeans(numbers, means);
  auto endTime = high_resolution_clock::now();
  double time1 = duration<double>(endTime - startTime).count();
  cout << "ComputeMeans: " << time1 << endl;
  Means pmeans = {};
  startTime = high_resolution_clock::now();
  ParallelComputeMeans(numbers, pmeans);
  endTime = high_resolution_clock::now();
  double time2 = duration<double>(endTime - startTime).count();
  cout << "ParallelComputeMeans: " << time2 << " (Speedup: " << time1 / time2 << ")\n";
}

int main() {
  TestFunctional();
#ifdef NDEBUG
  TestRuntime();
#endif
  return 0;
}
