#include <ctime>
#include <fstream>
#include <functional>
#include <iostream>
#include <random>
#include <string>


void Generate(std::string const &filename, int nrLines, int maxLen, int maxVal) {
    std::default_random_engine engine(static_cast<unsigned int>(time(0)));
    auto rgenLen = std::bind(std::uniform_int_distribution(0, maxLen), engine);
    auto rgenVal = std::bind(std::uniform_int_distribution(0, maxVal), engine);
    std::ofstream out(filename);
    for (int i = 0; i < nrLines; i++) {
        int len = rgenLen();
        for (int j = 0; j < len; j++) {
            out << rgenVal() << " ";
        }
        out << std::endl;
    }
}

int main(int argc, char *argv[]) {
    std::string filename;
    if (argc > 1) {  // filename passed as command-line parameter
        filename = argv[1];
    } else {  // read filename from cin
        std::cout << "Filename: ";
        std::cin >> filename;
    }
    int nrLines = 0;
    int maxLen  = 0;
    int maxVal  = 0;
    std::cout << "Nr. lines: ";
    std::cin >> nrLines;
    std::cout << "Max. line length: ";
    std::cin >> maxLen;
    std::cout << "Max. value: ";
    std::cin >> maxVal;
    Generate(filename, nrLines, maxLen, maxVal);
    return 0;
}
