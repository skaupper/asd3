#include <algorithm>
#include <condition_variable>
#include <fstream>
#include <iostream>
#include <string>
#include <tbb/parallel_sort.h>
#include <tbb/pipeline.h>
#include <thread>
#include <vector>
#include <chrono>
#include <atomic>
#include <variant>


bool ExtractNumbers(const std::string &str, std::vector<int> &numbers) {
    long long number = 0;
    bool isNumber    = false;
    for (size_t i = 0; i < str.size(); i++) {
        char ch = str[i];
        if (ch == ' ' || ch == '\t' || ch == '\r' || ch == '\n') {
            if (isNumber) {
                numbers.push_back(int(number));
                isNumber = false;
                number   = 0;
            }
        } else if (ch >= '0' && ch <= '9') {
            isNumber = true;
            number   = number * 10 + ch - '0';
            if (number > std::numeric_limits<int>::max()) {
                return false;
            }
        } else {
            return false;
        }
    }

    if (isNumber) {
        numbers.push_back(static_cast<int>(number));
    }
    return true;
}


std::string ConvertToString(const std::vector<int> &numbers) {
    std::string str;
    str.reserve(numbers.size() * 11);
    for (int number : numbers) {
        int d = 1;
        while (d <= number / 10) {
            d *= 10;
        }
        while (d > 0) {
            str += char(number / d + '0');
            number %= d;
            d /= 10;
        }
        str += ' ';
    }
    return str;
}


// ---------------------------------------------------------------------------------------------------------------------

class EndOfInput {};

using PipelineType = std::variant<EndOfInput, std::unique_ptr<std::string>>;

class EndOfInputSignal {
    std::atomic_bool readDone {false};
    std::atomic_bool writeStarted {false};

public:
    void signalReadFinished() { readDone.store(true); }
    bool isReadFinished() { return readDone.load(); }
    bool startWritingIfPossible() { return readDone && !writeStarted.exchange(true); }
};


class ReadAndExtractFunctor {
public:
    explicit ReadAndExtractFunctor(std::ifstream &in, EndOfInputSignal &sig) : mIn {in}, mSig {sig} {}

    PipelineType operator()(tbb::flow_control &fc) const {
        if (mSig.isReadFinished()) {
            fc.stop();
            return nullptr;
        }

        std::string line;
        if (!std::getline(mIn, line)) {
            mSig.signalReadFinished();
            return EndOfInput{};
        }

        return std::make_unique<std::string>(line);
    }


private:
    std::ifstream &mIn;
    EndOfInputSignal &mSig;
};

class SortNumbersFunctor {
public:
    PipelineType operator()(PipelineType param) const {
        if(std::holds_alternative<EndOfInput>(param)) {
            return param;
        }

        auto &line{std::get<1>(param)};
        std::vector<int> numbers;
        if (!ExtractNumbers(*line, numbers)) {
            return nullptr;
        }

        tbb::parallel_sort(std::begin(numbers), std::end(numbers));

        *line = ConvertToString(numbers);
        return std::move(line);
    }
};


class WriteNumbersFunctor {
public:
    explicit WriteNumbersFunctor(std::ofstream &out, EndOfInputSignal &sig, std::vector<std::string> &tempOutput)
        : mOut {out}, mSig {sig}, mTempOutput {tempOutput} {}

    void operator()(PipelineType param) const {
        if (std::holds_alternative<EndOfInput>(param)) {
            printStoredLines();
            return;
        }

        auto &sortedLine{std::get<1>(param)};

        if (!mSig.isReadFinished()) {
            if (sortedLine) {
                mTempOutput.push_back(*sortedLine);
            } else {
                mTempOutput.push_back("*** error ***");
            }
            return;
        }

        printStoredLines();

        if (sortedLine) {
            mOut << *sortedLine << std::endl;
        } else {
            mOut << "*** error ***" << std::endl;
        }
    }

private:

    void printStoredLines() const {
        if (mSig.startWritingIfPossible()) {
            std::copy(std::cbegin(mTempOutput), std::cend(mTempOutput), std::ostream_iterator<std::string>(mOut, "\n"));
            mTempOutput.clear();
        }
    }


    std::ofstream &mOut;
    EndOfInputSignal &mSig;
    std::vector<std::string> &mTempOutput;
};


// ---------------------------------------------------------------------------------------------------------------------


void Run(const std::string &inputName, const std::string &outputName) {
    std::ifstream in(inputName);
    std::ofstream out(outputName);
    std::string line;
    while (std::getline(in, line)) {
        std::vector<int> numbers;
        if (ExtractNumbers(line, numbers)) {
            std::sort(numbers.begin(), numbers.end());
            out << ConvertToString(numbers) << std::endl;
        } else {
            out << "*** error ***" << std::endl;
        }
    }
}

void RunParallel(const std::string &inputName, const std::string &outputName) {
    constexpr int ntoken {30};

    std::ifstream in {inputName};
    std::ofstream out {outputName};

    if (!in) {
        std::cerr << "Failed to open input file '" << inputName << "'." << std::endl;
        return;
    }
    if (!out) {
        std::cerr << "Failed to open output file '" << outputName << "'." << std::endl;
        return;
    }


    EndOfInputSignal sig;
    std::vector<std::string> tempOutput;

    const auto stage1 {tbb::make_filter<void, PipelineType>(tbb::filter::serial_in_order, ReadAndExtractFunctor {in, sig})};
    const auto stage2 {tbb::make_filter<PipelineType, PipelineType>(tbb::filter::parallel, SortNumbersFunctor {})};
    const auto stage3 {tbb::make_filter<PipelineType, void>(tbb::filter::serial_in_order, WriteNumbersFunctor {out, sig, tempOutput})};
    const auto combinedFilter {stage1 & stage2 & stage3};

    tbb::parallel_pipeline(ntoken, combinedFilter);
}


int main(int argc, char *argv[]) {
    if (argc < 3) {
        std::cout << "Not enough arguments.\n";
        return 1;
    }

    auto before {std::chrono::high_resolution_clock::now()};
    Run(argv[1], argv[2]);
    auto after {std::chrono::high_resolution_clock::now()};
    double time1 = std::chrono::duration<double>(after - before).count();

    before = std::chrono::high_resolution_clock::now();
    RunParallel(argv[1], argv[2]);
    after = std::chrono::high_resolution_clock::now();
    double time2 = std::chrono::duration<double>(after - before).count();

    std::cout << "Sequential: " << time1 << std::endl;
    std::cout << "Parallel  : " << time2 << " (Speedup: " << time1 / time2 << ")\n";

    return 0;
}
