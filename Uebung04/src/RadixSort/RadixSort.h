#ifndef RADIXSORT_H
#define RADIXSORT_H

#include <cassert>
#include <climits>
#include <execution>
#include <functional>
#include <iterator>
#include <memory>
#include <parallel/algorithm>
#include <tbb/parallel_for.h>
#include <tbb/parallel_reduce.h>
#include <tbb/task_group.h>
#include <vector>


namespace sorting {

    namespace internal {

        constexpr size_t radixBits = 8;
        constexpr size_t radix     = 1 << radixBits;

        template<typename TIter, typename TIter2>
        void RadixSort(TIter begin, TIter end, TIter2 temp, size_t digit) {
            using TValue  = typename std::iterator_traits<TIter>::value_type;
            using TValue2 = typename std::iterator_traits<TIter2>::value_type;

            const size_t size = static_cast<size_t>(end - begin);
            if (size > 1) {
                size_t count[radix] = {};
                for (TIter iter = begin; iter != end; ++iter) {  // count occurrences
                    assert(*iter >= 0);                          // this implementation only works for positive numbers
                    count[static_cast<long unsigned>(*iter >> digit) & (radix - 1)]++;
                }
                size_t pos[radix];  // positions of buckets in temporary array
                pos[0] = 0;
                for (size_t i = 0; i < radix - 1; i++) {  // compute starting positions of buckets
                    pos[i + 1] = pos[i] + count[i];
                }
                for (TIter iter = begin; iter != end; iter++) {  // put numbers into buckets
                    TValue value         = *iter;
                    size_t index         = static_cast<long unsigned>(value >> digit) & (radix - 1);
                    *(temp + pos[index]) = value;
                    pos[index]++;
                }

                std::copy(temp, temp + size, begin);  // copy contents of buckets back to container
                if (digit > 0) {
                    size_t nextDigit = digit - radixBits;
                    TIter iter       = begin;
                    TIter2 iterTemp  = temp;
                    for (size_t i = 0; i < radix; i++) {  // recursively sort subranges
                        size_t nr = count[i];
                        RadixSort(iter, iter + static_cast<TValue>(nr), iterTemp, nextDigit);
                        iter += static_cast<TValue>(nr);
                        iterTemp += static_cast<TValue2>(nr);
                    }
                }
            }
        }

    }  // namespace internal

    template<typename TIter>
    void RadixSort(TIter begin, TIter end) {
        using TValue = typename std::iterator_traits<TIter>::value_type;
        if (begin != end) {
            const size_t size = static_cast<size_t>(end - begin);
            if (size > 0) {
                std::unique_ptr<TValue[]> temp(new TValue[size]);
                internal::RadixSort(begin, end, temp.get(), sizeof(TValue) * CHAR_BIT - internal::radixBits);
            }
        }
    }

    namespace internal {

        template<typename TIter, typename TIter2>
        void ParallelRadixSort(TIter begin, TIter end, TIter2 temp, size_t digit, tbb::task_group &tg) {
            using TValue  = typename std::iterator_traits<TIter>::value_type;
            using TValue2 = typename std::iterator_traits<TIter2>::value_type;
            using Range   = tbb::blocked_range<TIter>;

            const size_t grainSize = 10000;
            if (static_cast<size_t>(end - begin) < grainSize) {
                RadixSort(begin, end, temp, digit);
                return;
            }

            const size_t size = static_cast<size_t>(end - begin);
            if (size > 1) {
                // Prevent the parallel_reduce to start threads for very small problem sizes
                constexpr size_t countGrainSize {10};
                // This version appears to be very slightly better than the original (much easier to read) for-loop.
                const std::array<size_t, radix> count = tbb::parallel_reduce(
                    Range {begin, end, countGrainSize}, std::array<size_t, radix> {{0}},

                    // 1) Create counts in subranges
                    [=](const Range &range, const std::array<size_t, radix> &partialCount) {
                        std::array<size_t, radix> localCount {partialCount};
                        for (auto iter {range.begin()}; iter != range.end(); ++iter) {  // count occurrences
                            assert(*iter >= 0);  // this implementation only works for positive numbers
                            localCount[static_cast<long unsigned>(*iter >> digit) & (radix - 1)]++;
                        }
                        return localCount;
                    },
                    // 2) Combine results from multiple subranges
                    [=](const std::array<size_t, radix> &c1, const std::array<size_t, radix> &c2) {
                        std::array<size_t, radix> result {c1};
                        for (size_t i = 0; i < radix; ++i) {
                            result[i] += c2[i];
                        }
                        return result;
                    });


                size_t pos[radix];  // positions of buckets in temporary array
                pos[0] = 0;
                for (size_t i = 0; i < radix - 1; i++) {  // compute starting positions of buckets
                    pos[i + 1] = pos[i] + count[i];
                }

                for (auto iter {begin}; iter != end; iter++) {  // put numbers into buckets
                    TValue value         = *iter;
                    size_t index         = static_cast<long unsigned>(value >> digit) & (radix - 1);
                    *(temp + pos[index]) = value;
                    pos[index]++;
                }

                std::copy(std::execution::par_unseq, temp, temp + size,
                          begin);  // copy contents of buckets back to container
                if (digit > 0) {
                    const size_t nextDigit = digit - radixBits;
                    TIter iter             = begin;
                    TIter2 iterTemp        = temp;
                    for (size_t i = 0; i < radix; i++) {  // recursively sort subranges
                        const size_t nr = count[i];
                        tg.run([=, &tg] {
                            ParallelRadixSort(iter, iter + static_cast<TValue>(nr), iterTemp, nextDigit, tg);
                        });
                        iter += static_cast<TValue>(nr);
                        iterTemp += static_cast<TValue2>(nr);
                    }
                }
            }
        }

    }  // namespace internal

    template<typename TIter>
    void ParallelRadixSort(TIter begin, TIter end) {
        using TValue = typename std::iterator_traits<TIter>::value_type;
        if (begin != end) {
            const size_t size = static_cast<size_t>(end - begin);
            if (size > 0) {
                tbb::task_group tg;
                std::unique_ptr<TValue[]> temp(new TValue[size]);
                internal::ParallelRadixSort(begin, end, temp.get(), sizeof(TValue) * CHAR_BIT - internal::radixBits,
                                            tg);
                tg.wait();
            }
        }
    }

}  // namespace sorting

#endif
