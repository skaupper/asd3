#include "RadixSort.h"
#include <algorithm>
#include <chrono>
#include <ctime>
#include <iostream>
#include <iterator>
#include <random>
#include <string>
#include <tbb/parallel_sort.h>
#include <vector>

using namespace std;
using namespace std::chrono;
using namespace tbb;
using namespace sorting;


size_t const smallN = 50000;
size_t const largeN = 50000000;

using Numbers = vector<int>;

void InitRandom(Numbers &vec, size_t const nr, int max) {
    auto rgen = std::bind(uniform_int_distribution(1, max + 1), default_random_engine());
    vec.reserve(nr);
    generate_n(back_inserter(vec), nr, rgen);
}

bool IsCorrectlySorted(Numbers numbers, Numbers const &sorted) {
    sort(numbers.begin(), numbers.end());
    return numbers == sorted;
}

void TestFunctional() {
    Numbers numbers;
    InitRandom(numbers, smallN, smallN);
    Numbers numbers1 = numbers;
    Numbers numbers2 = numbers;
    RadixSort(numbers1.begin(), numbers1.end());
    cout << "IsCorrectlySorted returned " << boolalpha << IsCorrectlySorted(numbers, numbers1) << endl;
    ParallelRadixSort(numbers2.begin(), numbers2.end());
    cout << "IsCorrectlySorted returned " << boolalpha << IsCorrectlySorted(numbers, numbers2) << endl;
}

typedef void (*SortFunction)(Numbers::iterator, Numbers::iterator);

void Measure(std::string const &name, Numbers const &numbers, SortFunction f) {
    Numbers sortedNumbers = numbers;
    auto start            = high_resolution_clock::now();
    f(sortedNumbers.begin(), sortedNumbers.end());
    double time = duration<double>(high_resolution_clock::now() - start).count();
    cout << name << time << endl;
}

void TestRuntime() {
    Numbers numbers;
    InitRandom(numbers, largeN, largeN);
    Measure("std::sort: ", numbers, sort<Numbers::iterator>);
    Measure("tbb::parallel_sort: ", numbers, parallel_sort<Numbers::iterator>);
    Measure("RadixSort: ", numbers, RadixSort<Numbers::iterator>);
    Measure("ParallelRadixSort: ", numbers, ParallelRadixSort<Numbers::iterator>);
}

int main() {
    TestFunctional();
#ifdef NDEBUG
    TestRuntime();
#endif
    return 0;
}
