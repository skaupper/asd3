#include "Means.h"

#include <algorithm>
#include <cassert>
#include <cmath>
#include <functional>
#include <tbb/concurrent_vector.h>
#include <tbb/parallel_invoke.h>
#include <tbb/parallel_reduce.h>
#include <tbb/parallel_sort.h>

#include <iostream>


namespace {

    int ComputeMode(const Numbers &sortedNumbers) {
        int mode        = sortedNumbers[0];
        size_t maxCount = 1;
        size_t i        = 0;
        size_t size     = sortedNumbers.size();
        while (i < size) {
            size_t j = i + 1;
            int nr   = sortedNumbers[i];
            while (j < size && nr == sortedNumbers[j]) {
                j++;
            }
            assert(j == size || nr < sortedNumbers[j]);
            size_t count = j - i;
            if (count > maxCount) {
                maxCount = count;
                mode     = nr;
            }
            i = j;
        }
        return mode;
    }

    double ComputeMedian(const Numbers &sortedNumbers) {
        size_t size = sortedNumbers.size();
        size_t m    = size / 2;
        return size % 2 != 0 ? sortedNumbers[m] : (sortedNumbers[m] + sortedNumbers[m - 1]) / 2.0;
    }

    void ComputeCommonMeans(const Numbers &numbers, Means &means) {
        double sum           = 0;
        double logSum        = 0;
        double reciprocalSum = 0;
        double quadSum       = 0;
        for (auto nr : numbers) {
            sum += nr;
            logSum += log(nr);
            reciprocalSum += 1.0 / nr;
            quadSum += nr * nr;
        }
        double size          = static_cast<double>(numbers.size());
        means.arithmeticMean = sum / size;
        means.geometricMean  = exp(logSum / size);
        means.harmonicMean   = size / reciprocalSum;
        means.quadraticMean  = sqrt(quadSum / size);
    }


    // -----------------------------------------------------------------------------------------------------------------

    template<typename INT>
    struct NumberCountPair {
        size_t count;
        INT value;
    };

    template<typename INT>
    struct PartialModeResult {
        NumberCountPair<INT> partialMode;
        NumberCountPair<INT> lowerBound;
        NumberCountPair<INT> upperBound;
    };


    int ParallelComputeMode(const Numbers &sortedNumbers) {
        using Range = tbb::blocked_range<Numbers::const_iterator>;

        using ValueType     = typename Numbers::value_type;
        using PartialResult = PartialModeResult<ValueType>;

        // The calculation of the mode cannot be split nicely, so we will calculate intermediate
        // results which must be merged later on.

        tbb::concurrent_vector<PartialResult> partialResults;

        // Calculates the intermediate result for the given range.
        // The result consists of three (value, count) pairs:
        //  1. The value at the beginning of the range may have further occurrences in other subranges and must be
        //  considered while merging.
        //  2. The intermediate mode value for the given range (which value occurrs most often in the given subrange?)
        //  3. The value at the end of the range may have further occurrences in other subranges and must be considered
        //  while merging.
        const auto computePartialMode = [&partialResults](const Range &range) {
            PartialResult &res {*partialResults.emplace_back()};
            bool firstSection {true};

            auto it = range.begin();
            while (it != range.end()) {
                auto it2     = it + 1;
                const int nr = *it;
                while (it2 != range.end() && nr == *it2) {
                    it2++;
                }
                assert(it2 == range.end() || nr < *it2);
                const size_t count = static_cast<size_t>(std::distance(it, it2));

                if (firstSection) {
                    res.lowerBound.count = count;
                    res.lowerBound.value = nr;
                    firstSection         = false;
                }

                if (it2 == range.end()) {
                    res.upperBound.count = count;
                    res.upperBound.value = nr;
                }

                if (count > res.partialMode.count) {
                    res.partialMode.count = count;
                    res.partialMode.value = nr;
                }

                it = it2;
            }
        };

        // With a grainsize of size/4, at most 4 threads should be spawned. Since the merging of the subresults scales
        // very badly with the number of subresults, higher values may worsen the runtime!
        tbb::parallel_for(Range {std::cbegin(sortedNumbers), std::cend(sortedNumbers), std::size(sortedNumbers) / 4},
                          computePartialMode);


        // Sort the subresults. The output should represent the order in which the corresponding subranges appear in the
        // input array.
        tbb::parallel_sort(std::begin(partialResults), std::end(partialResults), [](const auto &p1, const auto &p2) {
            if (p1.lowerBound.value != p2.lowerBound.value) {
                return p1.lowerBound.value < p2.lowerBound.value;
            }
            return p1.lowerBound.count > p2.lowerBound.count;
        });


        // Following procedure is used to merge subresults:
        //   1.) If the values of upperbound of the current result and the value of the lowerbound of the
        //       next subresult match, add up their number of occurrences.
        //   2.) If this count exceeds the count of the current mode, replace it.
        //   3.) If the count of the mode of the next subresult exceeds the count of the current mode, replace it.
        //   4.) Update the current upper bound, with the upper bound of the next result.
        //   5.) In case the values for upperbound and mode match, the count of the upperbound must be replaced with the
        //       count of the mode, since it may have been updated in step 1.
        PartialResult res {partialResults.front()};
        for (size_t i = 1; i < std::size(partialResults); ++i) {
            const auto ele {partialResults[i]};

            // 1.)
            NumberCountPair<ValueType> combinedBound;
            combinedBound.value = res.upperBound.value;
            combinedBound.count = res.upperBound.count + ele.lowerBound.count;
            assert(ele.lowerBound.value >= res.upperBound.value);
            if (ele.lowerBound.value != res.upperBound.value) {
                combinedBound.count = 0;
            }

            // 2.)
            if (combinedBound.count > res.partialMode.count) {
                res.partialMode = combinedBound;
            }

            // 3.)
            if (ele.partialMode.count > res.partialMode.count) {
                res.partialMode = ele.partialMode;
            }

            // 4.)
            res.upperBound = ele.upperBound;

            // 5.)
            if (res.upperBound.value == res.partialMode.value) {
                res.upperBound.count = res.partialMode.count;
            }
        }


        // Determine the actual result
        auto maxCount {res.lowerBound.count};
        auto mode {res.lowerBound.value};

        if (res.partialMode.count > maxCount) {
            maxCount = res.partialMode.count;
            mode     = res.partialMode.value;
        }
        if (res.upperBound.count > maxCount) {
            maxCount = res.upperBound.count;
            mode     = res.upperBound.value;
        }

        return mode;
    }


    void ParallelComputeCommonMeans(const Numbers &numbers, Means &means) {
        using Sums  = Means;
        using Range = tbb::blocked_range<Numbers::const_iterator>;

        constexpr auto computePartialSum = [](const Range &range, Sums sums) {
            for (auto it {range.begin()}; it != range.end(); ++it) {
                sums.arithmeticMean += *it;
                sums.geometricMean += log(*it);
                sums.harmonicMean += 1.0 / *it;
                sums.quadraticMean += *it * *it;
            }
            return sums;
        };

        constexpr auto combineSums = [](const Sums &s1, const Sums &s2) {
            Sums result {s1};
            result.arithmeticMean += s2.arithmeticMean;
            result.geometricMean += s2.geometricMean;
            result.harmonicMean += s2.harmonicMean;
            result.quadraticMean += s2.quadraticMean;
            return result;
        };


        const auto sums {
            tbb::parallel_reduce(Range {numbers.cbegin(), numbers.cend()}, Sums {}, computePartialSum, combineSums)};

        const size_t size {std::size(numbers)};
        means.arithmeticMean = sums.arithmeticMean / static_cast<double>(size);
        means.geometricMean  = exp(sums.geometricMean / static_cast<double>(size));
        means.harmonicMean   = static_cast<double>(size) / sums.harmonicMean;
        means.quadraticMean  = sqrt(sums.quadraticMean / static_cast<double>(size));
    }

}  // namespace


bool ComputeMeans(const Numbers &numbers, Means &means) {
    if (numbers.empty()) {
        return false;
    }
    Numbers sortedNumbers = numbers;
    std::sort(sortedNumbers.begin(), sortedNumbers.end());  // sorting required for mode and median
    means.mode   = ComputeMode(sortedNumbers);
    means.median = ComputeMedian(sortedNumbers);
    ComputeCommonMeans(numbers, means);
    return true;
}

bool ParallelComputeMeans(const Numbers &numbers, Means &means) {
    if (numbers.empty()) {
        return false;
    }

    tbb::parallel_invoke([&means, &numbers]() { ParallelComputeCommonMeans(numbers, means); },
                         [&means, &numbers]() {
                             Numbers sortedNumbers {numbers};
                             tbb::parallel_sort(std::begin(sortedNumbers), std::end(sortedNumbers));

                             //  means.mode = ComputeMode(sortedNumbers); (void)ParallelComputeMode;
                             means.mode   = ParallelComputeMode(sortedNumbers);
                             means.median = ComputeMedian(sortedNumbers);
                         });

    return true;
}
