#include "Means.h"
#include <algorithm>
#include <chrono>
#include <ctime>
#include <functional>
#include <iostream>
#include <iterator>
#include <random>


const size_t smallN = 20;
const size_t largeN = 100000000;

void InitRandom(Numbers &vec, size_t const nr, int max) {
    auto rgen = std::bind(std::uniform_int_distribution<int>(1, max + 1),
                          std::default_random_engine(static_cast<unsigned int>(time(0))));
    vec.reserve(nr);
    generate_n(back_inserter(vec), nr, rgen);
}

void PrintNumbers(const Numbers &numbers) {
    std::cout << "Numbers: ";
    std::copy(numbers.begin(), numbers.end(), std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;
}

void PrintMeans(const Means &means) {
    std::cout << "Arithmetic mean: " << means.arithmeticMean << std::endl
              << "Geometric mean: " << means.geometricMean << std::endl
              << "Harmonic mean: " << means.harmonicMean << std::endl
              << "Quadratic mean: " << means.quadraticMean << std::endl
              << "Median: " << means.median << std::endl
              << "Mode: " << means.mode << std::endl;
}

void TestFunctional() {
    Numbers numbers;
    InitRandom(numbers, smallN, smallN);

    Means means = {};
    ComputeMeans(numbers, means);
    PrintNumbers(numbers);
    std::cout << "\nComputeMeans:\n";
    PrintMeans(means);

    std::cout << "\nParallelComputeMeans:\n";
    Means pmeans = {};
    ParallelComputeMeans(numbers, pmeans);
    PrintMeans(pmeans);
}

void TestRuntime() {
    Numbers numbers;
    InitRandom(numbers, largeN, largeN);
    std::cout << "\nRuntime:\n";

    Means means {};
    auto startTime = std::chrono::high_resolution_clock::now();
    ComputeMeans(numbers, means);
    auto endTime = std::chrono::high_resolution_clock::now();
    double time1 = std::chrono::duration<double>(endTime - startTime).count();
    std::cout << "ComputeMeans: " << time1 << std::endl;

    Means pmeans = {};
    startTime    = std::chrono::high_resolution_clock::now();
    ParallelComputeMeans(numbers, pmeans);
    endTime      = std::chrono::high_resolution_clock::now();
    double time2 = std::chrono::duration<double>(endTime - startTime).count();
    std::cout << "ParallelComputeMeans: " << time2 << " (Speedup: " << time1 / time2 << ")\n";
}

int main() {
    TestFunctional();
#ifdef NDEBUG
    TestRuntime();
#endif
    return 0;
}
