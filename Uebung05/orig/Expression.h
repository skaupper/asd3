#ifndef EXPRESSION_H
#define EXPRESSION_H

using ValueType = double;

class Expression {
public:
  virtual ~Expression() {}
  virtual ValueType Evaluate() const = 0;
};

#endif
