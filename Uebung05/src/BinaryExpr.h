#ifndef BINARYEXPR_H
#define BINARYEXPR_H

#include "Expression.h"
#include "SizedMemoryPools.h"
#include <cmath>
#include <functional>
#include <memory>


template<typename Op>
class BinaryExpr : public Expression {
public:
    BinaryExpr(std::unique_ptr<Expression> &&expr1, std::unique_ptr<Expression> &&expr2)
        : mExpr1(std::move(expr1)), mExpr2(std::move(expr2)) {}

    virtual ValueType Evaluate() const override { return Op()(mExpr1->Evaluate(), mExpr2->Evaluate()); }

private:
    std::unique_ptr<Expression> mExpr1;
    std::unique_ptr<Expression> mExpr2;
};

using AddExpr  = BinaryExpr<std::plus<ValueType>>;
using SubExpr  = BinaryExpr<std::minus<ValueType>>;
using MultExpr = BinaryExpr<std::multiplies<ValueType>>;
using DivExpr  = BinaryExpr<std::divides<ValueType>>;

template<typename T>
struct Power {
    double operator()(T x, T y) const { return std::pow(x, y); }
};

using PowExpr = BinaryExpr<Power<ValueType>>;


#endif
