#ifndef EXPRESSION_H
#define EXPRESSION_H

#include "SizedMemoryPools.h"


template<typename T>
class UnaryExpr;
template<typename T>
class BinaryExpr;
class Number;


using ValueType = double;

class Expression : public SizedPoolAllocation<UnaryExpr<void>, BinaryExpr<void>, Number> {
public:
    virtual ~Expression() {}
    virtual ValueType Evaluate() const = 0;
};


#include "BinaryExpr.h"
#include "Number.h"
#include "UnaryExpr.h"

#endif
