#ifndef NUMBER_H
#define NUMBER_H

#include "Expression.h"
#include "SizedMemoryPools.h"


class Number : public Expression {
public:
    explicit Number(ValueType value) : mValue(value) {}
    virtual ValueType Evaluate() const override { return mValue; }

private:
    ValueType mValue;
};

#endif
