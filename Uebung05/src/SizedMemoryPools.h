#ifndef SIZED_MEMORY_POOLS_H
#define SIZED_MEMORY_POOLS_H

#include <boost/pool/singleton_pool.hpp>


template<size_t SIZE>
struct PoolSizeTag {};

template<size_t SIZE>
class SizedMemoryPool {
private:
    using SizedPool = boost::singleton_pool<PoolSizeTag<SIZE>, SIZE>;

public:
    static void *malloc() { return SizedPool::malloc(); }
    static void free(void *ptr) { SizedPool::free(ptr); }
};


template<typename... T>
struct SizedPoolAllocation {
private:
    template<size_t S, size_t... SIZES>
    static void *alloc(size_t size) {
        if (size == S) {
            return SizedMemoryPool<S>::malloc();
        }
        if constexpr (sizeof...(SIZES) == 0) {
            return ::operator new(size);
        } else {
            return alloc<SIZES...>(size);
        }
    }

    template<size_t S, size_t... SIZES>
    static void free(void *ptr, size_t size) {
        if (size == S) {
            SizedMemoryPool<S>::free(ptr);
            return;
        }
        if constexpr (sizeof...(SIZES) == 0) {
            ::operator delete(ptr, size);
        } else {
            free<SIZES...>(ptr, size);
        }
    }

public:
    void *operator new(size_t size) { return alloc<sizeof(T)...>(size); }
    void operator delete(void *ptr, size_t size) { free<sizeof(T)...>(ptr, size); }
};


#endif
