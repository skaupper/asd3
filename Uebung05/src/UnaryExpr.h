#ifndef UNARYEXPR_H
#define UNARYEXPR_H

#include "Expression.h"
#include "SizedMemoryPools.h"
#include <functional>
#include <memory>


template<typename Op>
class UnaryExpr : public Expression {
public:
    explicit UnaryExpr(std::unique_ptr<Expression> &&expr) : mExpr(std::move(expr)) {}
    virtual ValueType Evaluate() const { return Op()(mExpr->Evaluate()); }

private:
    std::unique_ptr<Expression> mExpr;
};

using NegativeExpr = UnaryExpr<std::negate<ValueType>>;

#endif
