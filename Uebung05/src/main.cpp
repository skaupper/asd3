#include "Calculator.h"

#include <iostream>
#include <string>


#ifdef _MSC_VER
#include <crtdbg.h>
#endif

namespace {

    void Run() {
        Calculator calc;
        while (calc.IsRunning()) {
            std::cout << "> ";
            std::string cmd;
            std::getline(std::cin, cmd);
            calc.Execute(cmd);
        }
    }

}  // namespace


int main() {
    // enable memory leak detection under MSVC
#if defined(_MSC_VER) && defined(_DEBUG)
    _CrtSetDbgFlag(_CrtSetDbgFlag(_CRTDBG_REPORT_FLAG) | _CRTDBG_LEAK_CHECK_DF);
#endif

    Run();
    return 0;
}
